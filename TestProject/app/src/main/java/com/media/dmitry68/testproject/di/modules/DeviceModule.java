package com.media.dmitry68.testproject.di.modules;

import com.media.dmitry68.testproject.data.device.DeviceInfo;
import com.media.dmitry68.testproject.device.DeviceInfoImpl;
import com.media.dmitry68.testproject.presentation.postdevice.PostDevicePresenter;
import com.media.dmitry68.testproject.presentation.postdevice.PostDevicePresenterImpl;
import com.media.dmitry68.testproject.usecases.postdevice.PostDevice;
import com.media.dmitry68.testproject.device.PostDeviceImpl;
import com.media.dmitry68.testproject.usecases.postdevice.PostDeviceScope;

import dagger.Module;
import dagger.Provides;

@Module
public class DeviceModule {

    @Provides
    @PostDeviceScope
    public PostDevice provideInteractor(PostDeviceImpl interactor) {
        return interactor;
    }

    @Provides
    @PostDeviceScope
    public PostDevicePresenter providePresenter(PostDevicePresenterImpl presenter) {
        return presenter;
    }

    @Provides
    @PostDeviceScope
    public DeviceInfo provideDeviceInfo(DeviceInfoImpl deviceInfo) {
        return deviceInfo;
    }
}
