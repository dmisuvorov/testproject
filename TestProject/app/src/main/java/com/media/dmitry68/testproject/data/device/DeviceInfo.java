package com.media.dmitry68.testproject.data.device;

import com.media.dmitry68.testproject.domain.model.Device;

public interface DeviceInfo {
    Device getDevice();
}
