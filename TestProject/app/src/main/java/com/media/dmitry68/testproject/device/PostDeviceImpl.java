package com.media.dmitry68.testproject.device;

import com.media.dmitry68.testproject.data.client.TestApi;
import com.media.dmitry68.testproject.domain.model.Device;
import com.media.dmitry68.testproject.usecases.postdevice.PostDevice;
import com.media.dmitry68.testproject.usecases.postdevice.PostDeviceScope;

import javax.inject.Inject;

import retrofit2.Call;

@PostDeviceScope
public class PostDeviceImpl implements PostDevice {
    private TestApi api;

    @Inject
    public PostDeviceImpl(TestApi api) {
        this.api = api;
    }

    @Override
    public Call<Device> postDevice(Device device) {
        return api.postDevice(device);
    }

}
