package com.media.dmitry68.testproject.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.media.dmitry68.testproject.di.ApplicationComponent;
import com.media.dmitry68.testproject.TestApplication;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        injectDependencies(TestApplication.get(this), TestApplication.getComponent());
    }

    protected abstract void injectDependencies(TestApplication application, ApplicationComponent component);

    @Override
    public void finish() {
        super.finish();

        releaseSubComponents(TestApplication.get(this));
    }

    protected abstract void releaseSubComponents(TestApplication application);
}
