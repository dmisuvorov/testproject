package com.media.dmitry68.testproject.di.modules;

import com.media.dmitry68.testproject.util.Constants;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;

@Module
public class ApplicationModule {

    @Provides
    @Singleton
    @Named("header")
    String provideHeader() {
        return Constants.HEADER;
    }

    @Provides
    @Singleton
    @Named("headerValue")
    String provideHeaderValuer() {
        return Constants.HEADER_VALUE;
    }

    @Provides
    @Singleton
    HttpUrl provideEndpoint() {
        return HttpUrl.parse(Constants.BASE_URL);
    }
}
