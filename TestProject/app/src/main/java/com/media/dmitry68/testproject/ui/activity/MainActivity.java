package com.media.dmitry68.testproject.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.media.dmitry68.testproject.di.ApplicationComponent;
import com.media.dmitry68.testproject.R;
import com.media.dmitry68.testproject.TestApplication;
import com.media.dmitry68.testproject.ui.fragments.PostDeviceFragment;

public class MainActivity extends BaseActivity {
    public static final String TAG_POST_DEVICE_FRAGMENT = "post_device_fragment";

    private PostDeviceFragment postDeviceFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            postDeviceFragment = PostDeviceFragment.newInstance();
            attachFragments();
        } else {
            postDeviceFragment = (PostDeviceFragment) getSupportFragmentManager().findFragmentByTag(TAG_POST_DEVICE_FRAGMENT);
        }
    }

    private void attachFragments() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.post_data_fragment, postDeviceFragment, TAG_POST_DEVICE_FRAGMENT);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    protected void injectDependencies(TestApplication application, ApplicationComponent component) {
        component.inject(this);
    }

    @Override
    protected void releaseSubComponents(TestApplication application) {
        application.releaseDeviceSubComponent();
    }


}
