package com.media.dmitry68.testproject.di.modules;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class ClientModule {

    @Singleton
    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor,
                                     Interceptor headerInterceptor) {

        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(headerInterceptor)
                .addInterceptor(loggingInterceptor);

        return okHttpClient.build();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    @Provides
    @Singleton
    Interceptor provideHeaderInterceptor(@Named("header") String header,
                                         @Named("headerValue") String headerValue) {
        return chain -> {
            Request request = chain.request()
                    .newBuilder()
                    .addHeader(header, headerValue)
                    .build();

            return chain.proceed(request);
        };
    }
}
