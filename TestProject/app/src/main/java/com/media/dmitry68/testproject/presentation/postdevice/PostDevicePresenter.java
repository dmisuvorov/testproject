package com.media.dmitry68.testproject.presentation.postdevice;

import com.media.dmitry68.testproject.presentation.BasePresenter;
import com.media.dmitry68.testproject.ui.fragments.PostDeviceView;

public interface PostDevicePresenter extends BasePresenter<PostDeviceView> {
    void postDevice(boolean isConnected);
}
