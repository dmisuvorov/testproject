package com.media.dmitry68.testproject.ui.fragments;

public interface PostDeviceView {
    boolean checkPermission(String permission);

    void requestPermission(String permission);

    void showMessage();

    void showOfflineMessage();

    void showError();

    void showProgress();

    void hideProgress();
}
