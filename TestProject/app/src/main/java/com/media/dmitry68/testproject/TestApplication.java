package com.media.dmitry68.testproject;

import android.app.Application;
import android.content.Context;

import com.media.dmitry68.testproject.di.ApplicationComponent;

import com.media.dmitry68.testproject.di.DaggerApplicationComponent;
import com.media.dmitry68.testproject.di.DeviceSubComponent;
import com.media.dmitry68.testproject.di.modules.AndroidModule;
import com.media.dmitry68.testproject.di.modules.DeviceModule;

public class TestApplication extends Application {
    private static ApplicationComponent component;

    private DeviceSubComponent deviceSubComponent;

    public static ApplicationComponent getComponent() {
        return component;
    }

    public static TestApplication get(Context context) {
        return (TestApplication) context.getApplicationContext();
    }

    public DeviceSubComponent getDeviceSubComponent() {
        if (deviceSubComponent == null) {
            createDeviceSubComponent();
        }
        return deviceSubComponent;
    }

    private void createDeviceSubComponent() {
        deviceSubComponent = component.plus(new DeviceModule());
    }

    public void releaseDeviceSubComponent() {
        deviceSubComponent = null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = createComponent();
    }

    private ApplicationComponent createComponent() {
        return DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();
    }
}
