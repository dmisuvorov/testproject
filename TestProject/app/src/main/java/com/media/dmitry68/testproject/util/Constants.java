package com.media.dmitry68.testproject.util;

public class Constants {

    public static final String BASE_URL = "http://some.url.com:80/";
    public static final String HEADER = "test-header";
    public static final String HEADER_VALUE = "test";
}
