package com.media.dmitry68.testproject.usecases.postdevice;

import com.media.dmitry68.testproject.domain.model.Device;

import retrofit2.Call;

public interface PostDevice {

    Call<Device> postDevice(Device device);
}
