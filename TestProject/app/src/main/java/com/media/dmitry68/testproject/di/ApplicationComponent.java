package com.media.dmitry68.testproject.di;

import com.media.dmitry68.testproject.di.modules.DeviceModule;
import com.media.dmitry68.testproject.ui.activity.MainActivity;
import com.media.dmitry68.testproject.di.modules.AndroidModule;
import com.media.dmitry68.testproject.di.modules.ApiModule;
import com.media.dmitry68.testproject.di.modules.ApplicationModule;
import com.media.dmitry68.testproject.di.modules.ClientModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AndroidModule.class,
        ApplicationModule.class,
        ApiModule.class,
        ClientModule.class
})
public interface ApplicationComponent {
    void inject(MainActivity activity);

    DeviceSubComponent plus(DeviceModule module);
}
