package com.media.dmitry68.testproject.di.modules;

import android.content.Context;
import android.content.res.Resources;

import com.media.dmitry68.testproject.TestApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AndroidModule {
    private TestApplication application;

    public AndroidModule(TestApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    Resources provideResources() {
        return application.getResources();
    }

}
