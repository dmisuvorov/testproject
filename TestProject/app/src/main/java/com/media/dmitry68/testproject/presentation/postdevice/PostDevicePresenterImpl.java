package com.media.dmitry68.testproject.presentation.postdevice;

import android.Manifest;

import com.media.dmitry68.testproject.data.device.DeviceInfo;
import com.media.dmitry68.testproject.domain.model.Device;
import com.media.dmitry68.testproject.ui.fragments.PostDeviceView;
import com.media.dmitry68.testproject.usecases.postdevice.PostDevice;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostDevicePresenterImpl implements PostDevicePresenter {
    @Inject
    PostDevice interactor;

    @Inject
    DeviceInfo deviceInfo;

    private PostDeviceView view;

    @Inject
    PostDevicePresenterImpl(){

    }

    @Override
    public void postDevice(boolean isConnected) {
        if (view != null) {
            if (view.checkPermission(Manifest.permission.READ_PHONE_STATE)) {
                if (isConnected) {
                    view.showProgress();
                    interactor.postDevice(deviceInfo.getDevice()).enqueue(new Callback<Device>() {
                        @Override
                        public void onResponse(Call<Device> call, Response<Device> response) {
                            if (response.isSuccessful()) {
                                if (view != null) {
                                    view.hideProgress();
                                    view.showMessage();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Device> call, Throwable t) {
                            if (view != null) {
                                view.hideProgress();
                                view.showError();
                            }
                        }
                    });
                } else {
                    view.showOfflineMessage();
                }
            } else {
                view.requestPermission(Manifest.permission.READ_PHONE_STATE);
            }
        }
    }

    @Override
    public void bind(PostDeviceView view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        view = null;
    }
}
