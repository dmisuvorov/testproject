package com.media.dmitry68.testproject.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.media.dmitry68.testproject.R;
import com.media.dmitry68.testproject.TestApplication;
import com.media.dmitry68.testproject.presentation.postdevice.PostDevicePresenter;
import com.media.dmitry68.testproject.util.Utils;

import java.util.Objects;

import javax.inject.Inject;

public class PostDeviceFragment extends BaseFragment implements PostDeviceView {
    private static final int REQUEST_PERMISSION_RESULT = 100;

    @Inject
    PostDevicePresenter presenter;
    @Inject
    Resources resources;
    @Inject
    Context context;

    private TextView textResult;
    private ProgressBar progressBar;

    public PostDeviceFragment() {

    }

    public static PostDeviceFragment newInstance() {
        return new PostDeviceFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter.bind(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_post_data, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        textResult = Objects.requireNonNull(getView()).findViewById(R.id.text_result);
        Button buttonPostData = getView().findViewById(R.id.button_post);
        progressBar = getView().findViewById(R.id.progress);

        buttonPostData.setOnClickListener(v -> onClickButton());
    }

    private void onClickButton() {
        presenter.postDevice(Utils.isConnected(context));
    }

    @Override
    public boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void requestPermission(String permission) {
        requestPermissions(new String[]{permission}, REQUEST_PERMISSION_RESULT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_RESULT) {
            presenter.postDevice(Utils.isConnected(context));
        }
    }

    @Override
    public void showMessage() {
        textResult.setText(resources.getString(R.string.success_message));
    }

    @Override
    public void showOfflineMessage() {
        Snackbar.make(Objects.requireNonNull(getView()), R.string.offline_message, Snackbar.LENGTH_LONG)
                .setAction(R.string.action_online, v -> startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS)))
                .setActionTextColor(Color.GREEN)
                .show();
    }

    @Override
    public void showError() {
        textResult.setText(resources.getString(R.string.error_message));
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.unbind();
    }

    @Override
    protected void injectDependencies(TestApplication application) {
        application.getDeviceSubComponent().inject(this);
    }
}
