package com.media.dmitry68.testproject.di;

import com.media.dmitry68.testproject.di.modules.DeviceModule;
import com.media.dmitry68.testproject.ui.fragments.PostDeviceFragment;
import com.media.dmitry68.testproject.usecases.postdevice.PostDeviceScope;

import dagger.Subcomponent;

@PostDeviceScope
@Subcomponent(modules = {
        DeviceModule.class
})
public interface DeviceSubComponent {

    void inject(PostDeviceFragment fragment);

}
