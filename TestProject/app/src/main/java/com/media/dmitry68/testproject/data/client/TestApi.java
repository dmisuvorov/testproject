package com.media.dmitry68.testproject.data.client;

import com.media.dmitry68.testproject.domain.model.Device;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface TestApi {

    @POST("/api/devices")
    Call<Device> postDevice(@Body Device device);
}
