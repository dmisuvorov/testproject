package com.media.dmitry68.testproject.device;

import android.content.Context;

import com.media.dmitry68.testproject.data.device.DeviceInfo;
import com.media.dmitry68.testproject.domain.model.Device;
import com.media.dmitry68.testproject.util.Utils;

import javax.inject.Inject;


public class DeviceInfoImpl implements DeviceInfo {
    private Context context;

    @Inject
    public DeviceInfoImpl(Context context) {
        this.context = context;
    }

    @Override
    public Device getDevice() {
        return new Device(getImei(), String.valueOf(getAndroidVersion()), getMobileModel(), getMobileManufacturer());
    }

    private String getImei() {
        return Utils.getImeiNumber(context);
    }

    private int getAndroidVersion() {
        return Utils.getAndroidVersion();
    }

    private String getMobileModel() {
        return Utils.getMobileModel();
    }

    private String getMobileManufacturer() {
        return Utils.getMobileManufacturer();
    }
}
