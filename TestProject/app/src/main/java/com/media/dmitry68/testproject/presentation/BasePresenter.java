package com.media.dmitry68.testproject.presentation;

public interface BasePresenter<T> {
    void bind(T view);

    void unbind();
}
