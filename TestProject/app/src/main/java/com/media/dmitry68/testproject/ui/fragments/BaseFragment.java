package com.media.dmitry68.testproject.ui.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.media.dmitry68.testproject.TestApplication;

public abstract class BaseFragment extends Fragment {
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        injectDependencies(TestApplication.get(getContext()));
    }

    protected abstract void injectDependencies(TestApplication application);
}
