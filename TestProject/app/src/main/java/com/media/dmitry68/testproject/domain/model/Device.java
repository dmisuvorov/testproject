package com.media.dmitry68.testproject.domain.model;

import com.google.gson.annotations.SerializedName;

public class Device {

    @SerializedName("imei")
    private String imei;

    @SerializedName("androidVersion")
    private String androidVersion;

    @SerializedName("mobileModel")
    private String mobileModel;

    @SerializedName("mobileManufacturer")
    private String mobileManufacturer;

    public Device(String imei, String androidVersion, String mobileModel, String mobileManufacturer) {
        this.imei = imei;
        this.androidVersion = androidVersion;
        this.mobileModel = mobileModel;
        this.mobileManufacturer = mobileManufacturer;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getMobileModel() {
        return mobileModel;
    }

    public void setMobileModel(String mobileModel) {
        this.mobileModel = mobileModel;
    }

    public String getMobileManufacturer() {
        return mobileManufacturer;
    }

    public void setMobileManufacturer(String mobileManufacturer) {
        this.mobileManufacturer = mobileManufacturer;
    }
}
